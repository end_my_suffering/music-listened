# Music Stats Page
PHP.

* Scrape music listen history from Libre.fm
* Display the history
* Generate stats

Hosted at https://1606.ovh/music
Designed for my personal usage only, will NOT work out-of-the-box.

# Requirements
  * PHP 7.0+, with following extension(s)
    * php-apcu
  * MySQL
  
# Installation (TBD)
  1. Install composer, `php -f composer.phar install`
  2. Copy `config/config.sample.php` to `config/config.php`
  3. Update `config/config.php`
  4. ... ? TBD

# Acknowledgment
  This software adopted the following software.
  
 * [PHP-MySQLi-Database-Class](https://github.com/ThingEngineer/PHP-MySQLi-Database-Class/)
 	* Copyright (C) 2013  Josh Campbell (ajillion)
 	* GPLv3
 * [PHP Curl Class](https://github.com/php-curl-class/php-curl-class)
 	* Public Domain
 * [Pure](https://github.com/pure-css/pure)
 	* Copyright 2013 Yahoo! Inc.
 	* BSD License

# License
    Music Stats Page
    Copyright (C) 2020  Adam S

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.