<?php

// This script is to generate a HTML from Librefm directly,
// without any other processing. 
// It is designed to run periodically in relatively short period, e.g. 1 hour.

$header = <<<HERE
<!doctype html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">

  <title>1606 Recent Played Music</title>
  <link rel="stylesheet" href="css/pure_2.0.3/pure-min.css" />
  <link rel="stylesheet" href="css/pure_2.0.3/grids-responsive-min.css" />
  <link rel="stylesheet" href="css/main.css" />
</head>

<body>

<div class="main">
  
<h1>1606 Recent Played Music</h1>

<table class="pure-table pure-g">
    <thead class="pure-u-1">
        <tr class="pure-u-1">
            <th class="pure-u-sm-3-8 pure-u-7-24">Artist</th>
            <th class="pure-u-sm-3-8 pure-u-1-3">Title</th>
            <th class="pure-u-1-24">Ago</th>
        </tr>
    </thead>
    <tbody class="pure-u-1">
HERE;

require __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/config/config.php';

use \Curl\Curl;

$curl = new Curl();

$curl->get('https://libre.fm/2.0/', array(
	'method'	=> 'user.getrecenttracks',
	'user'		=> LIBREFM_USER,
	'page'		=> '1',
	'limit'		=> '20',
	'format'	=> 'json'
	));
$fm_recenttracks = $curl->response;



//var_dump(json_decode($fm_recenttracks));
//var_export($fm_recenttracks);
$fm_recenttracks_array = $fm_recenttracks->{'recenttracks'}->{'track'};
//var_dump($fm_recenttracks_array);

$output = NULL;

foreach($fm_recenttracks_array as $track) {
	
	$date_raw = $track->{'date'}->{'uts'};
	$date = date(DATE_RFC1123,$date_raw);
	$date_elapsed = humanTiming($date_raw);
	$artist = $track->{'artist'}->{'#text'};
	$name = $track->{'name'};
	$album = $track->{'album'}->{'#text'};
	$url = $track->{'url'};
  $url_ddg = 'https://duckduckgo.com/?q='.urlencode('\\'.$name.' '.$artist.' site:youtube.com');
		
	if(!isset($track->{'@attr'}->{'nowplaying'})) {
	
$output .= <<<EOF

        <tr class="pure-u-1">
            <td class="pure-u-sm-3-8 pure-u-7-24">$artist</td>
            <td class="pure-u-sm-3-8 pure-u-1-3"><a href="$url_ddg">$name</a></td>
            <td class="pure-u-1-24">$date_elapsed</td>
        </tr>
EOF;

	}
}


function humanTiming ($time) // Credit: https://stackoverflow.com/a/2916189
{

    $time = time() - $time; // to get the time since that moment
    $time = ($time<1)? 1 : $time;
    $tokens = array (
        31536000 => 'year',
        2592000 => 'month',
        604800 => 'week',
        86400 => 'd',
        3600 => 'h',
        60 => 'm',
        1 => 's'
    );

    foreach ($tokens as $unit => $text) {
        if ($time < $unit) continue;
        $numberOfUnits = floor($time / $unit);
        return $numberOfUnits.$text;
    }

}

$footer = <<<HERE
    
		</tbody>
</table>

<p>Note:</p>
<p>1) Links are provided by <a href="https://duckduckgo.com">DuckDuckGo</a>, an search engine aims to protect your Privacy.</p>
<p>2) Links are then redirecting to Youtube. </p>
</div>
</body>
</html>
HERE;


$html = $header.$output.$footer;
file_put_contents(__DIR__ . '/public/index.html', $html);

?>